import {host, portNumber} from "./SystemSettings";

const URL = host+"/heuristic";

export const endpoint_uploadFile = URL +"/upload/reduceDimensionAndCluster/"

export const endpoint_cluster = URL +"/upload/cluster/"

export const endpoint_KeggPathway = URL +"/upload/keggPathway/"