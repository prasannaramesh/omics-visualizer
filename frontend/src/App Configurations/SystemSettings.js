export const host = "https://www.solar-omics-viewer.ovgu.de";
// export const host = "http://192.168.0.101"; //"http://127.0.0.1";

export const portNumber = 9004;

export const requestBodySize = 1000000;  //1MB